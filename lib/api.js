

const host = "http://127.0.0.1:8283/api/v1";
const request = require("request");
const rp = require('request-promise');


module.exports = {

    getInfo:  () => {
        return getApi('/ledger');
    },

    getNodeInfo:  () => {
        return getApi('/nodeinfo');
    },

    getPeerDB:  () => {
        return getApi('/peerdb');
    },

    getPeerInfo:  () => {
        return getApi('/peerinfo');
    },

    getBlockHash: (height)=>{
     
        return getApi(`/blockdb/getblockhash/${height}`);
    },

    getBlock: (hash, withTransactionOrNot)=> {

        return getApi(`/blockdb/get/${hash}/${withTransactionOrNot}`);
    },

    getBalance: (address)=>{
        
        return getApi(`/ledger/get/${address}`);
    }
}



function getApi(url){
    return new Promise((resolve) => {
        request({
            url: host + url,
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36'
            },
            forever: true,
            json: true
        }, (err, res, body)=>{
            resolve(body);
        });
    });
}



















