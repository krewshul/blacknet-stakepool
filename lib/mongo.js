// getting-started.js
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/stakepool', { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("we're connected!")
});

var WorkerSchema = new mongoose.Schema({
    address: String,
    hashrate: Number,
    payouts: Number
});

var BlockSchema = new mongoose.Schema({
    size: Number,
    version: String,
    previous: String,
    time: String,
    generator: String,
    contentHash: String,
    signature: String,
    transactions: Array,
    height: Number,
    blockHash: String,
    reward: String
});

var TransactionSchema = new mongoose.Schema({
    txid: String,
    size: Number,
    signature: String,
    from: String,
    seq: Number,
    blockHash: String,
    fee: Number,
    type: Number,
    data: Object,
    to: String,
    time: String,
    amount: Number,
    blockHeight: Number
});

global.Block = mongoose.model('Block', BlockSchema);
global.Transaction = mongoose.model('Transaction', TransactionSchema);
global.Worker = mongoose.model('Worker', WorkerSchema);