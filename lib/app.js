
global.config = require('../config');

const API = require('./api');
const explorerAPI = require('./explorerapi');
const Bignumber = require('bignumber.js');



async function syncTransactions() {

    let types = [0, 2, 3, 254], page = 1, address = config.account;
    let type = types.pop();
    console.log('start sync type=' + type);
    while (false) {

        let txns = await explorerAPI.getTransactions(address, type, page)
        console.log('start sync type=' + type + ' page=' + page);
        if (txns.length == 0) {

            if (types.length == 0) break;

            type = types.pop();


            console.log('start sync type=' + type);
            page = 1;
            continue;
        }

        for (let tx of txns) {

            delete tx._id;
            let instance;

            if (tx.txid) {
                instance = await Transaction.findOne({ txid: tx.txid });
            }

            if (type == 254) {
                instance = await Transaction.findOne({ type: 254, blockHash: tx.blockHash });
            }

            if (!instance) {
                instance = new Transaction(tx);
                instance.save();
            } else {
                console.log(instance.blockHash + ' is exists')
            }
        }
        page++;
    }

    initWorkers();

}

async function initWorkers() {

    await Worker.deleteMany({});
    let leaseTxns = await Transaction.find({ type: 2 });

    for (let tx of leaseTxns) {

        let address = tx.from, instance;

        if (address == config.account) continue;

        instance = await Worker.findOne({address});

        if(!instance){
            instance = new Worker({
                address,
                hashrate: tx.data.amount,
                payouts: 0
            });
        }else{
            instance.hashrate += tx.data.amount;
        }

        await instance.save();
    }

    let cancelLeaseTxns = await Transaction.find({ type: 3 });
    
    for (let tx of cancelLeaseTxns) {

        let address = tx.from, instance;

        if (address == config.account) continue;

        instance = await Worker.findOne({address});

        if(!instance){
            instance = new Worker({
                address,
                hashrate: tx.data.amount,
                payouts: 0
            });
        }else{
            instance.hashrate -= tx.data.amount;
            console.log(instance.hashrate)
        }

        await instance.save();
    }

}



syncTransactions();



setInterval(initOverview, 60 * 1000);

async function initOverview() {
    let ledger = await API.getInfo();
    let nodeInfo = await API.getNodeInfo();
    ledger.supply = new Bignumber(ledger.supply).dividedBy(1e8).toFixed(0);
    global.overview = {
        ...ledger,
        ...nodeInfo
    };
}

initOverview();
