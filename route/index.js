


const Router = require('koa-router');
const router = new Router();
const API = require('../lib/api');

router.get('/', async (ctx, next) => {

    
    await ctx.render('index', { overview, account: config.account});
});


router.get('/recent_blocks', async (ctx, next) => {

    
    await ctx.render('recent_blocks', { recent: true ,overview});
});

router.get('/workders', async (ctx, next) => {

    await ctx.render('workders', { workders: true ,overview});
});

router.get('/payouts', async (ctx, next) => {

    await ctx.render('payouts', { payouts: true ,overview});
});

module.exports = router;

